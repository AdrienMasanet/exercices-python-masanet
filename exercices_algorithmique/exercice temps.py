from datetime import datetime
import locale

locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')

current_time = datetime.now()

since = datetime(day=1, month=1, year=current_time.year)

time_difference = current_time - since

print(int(time_difference.total_seconds()/60), "minutes se sont écoulées depuis le", datetime.strftime(since, "%d/%m/%Y."))
print("Si on ajoutait ce temps écoulé à la date d'aujourd'hui, on serait le", datetime.strftime(current_time+time_difference, "%A %m %Y."))
