def sum_to(max):
    if max:
        return max + sum_to(max - 1)
    else:
        return 0


print(sum_to(5))

input()