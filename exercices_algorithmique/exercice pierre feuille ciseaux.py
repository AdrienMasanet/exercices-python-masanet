import random

available_actions = [
    "pierre",
    "feuille",
    "ciseaux"
]
last_player_action = ""
player_score = 0
last_computer_action = ""
computer_score = 0
win = loose = False


def player_choice():
    global last_player_action

    print("Écrivez : pierre, feuille ou ciseaux")
    last_player_action = input()


def check_player_action():
    global last_player_action

    if last_player_action in available_actions:
        print(f"vous jouez {last_player_action} !")
        return True
    else:
        print(f"l'action {last_player_action} n'existe pas dans la liste d'actions disponibles !\n")
        return False


def computer_choice():
    global last_computer_action

    last_computer_action = random.choice(available_actions)
    print(f"L'ennemi joue {last_computer_action} !")
    return last_computer_action


def round_loss():
    global computer_score

    print("Vous avez perdu ce round....\n")
    computer_score += 1
    pass


def round_won():
    global player_score

    print("Vous avez gagné ce round :D !!!\n")
    player_score += 1
    pass


def check_round_winner():
    global last_player_action, last_computer_action

    print(f"{last_player_action} contre {last_computer_action} !!!!")

    if last_player_action == last_computer_action:
        print(f"{last_player_action} contre {last_computer_action}, égalité pour ce round...")
    elif last_player_action == "pierre" and last_computer_action == "feuille":
        print("Votre pauvre caillou se fait envelopper par la feuille démoniaque ennemie !")
        round_loss()
    elif last_player_action == "pierre" and last_computer_action == "ciseaux":
        print("Votre pierre solide éclate les ciseaux ennemis !")
        round_won()
    elif last_player_action == "feuille" and last_computer_action == "pierre":
        print("Votre jolie feuille étouffe la pierre ennemie !")
        round_won()
    elif last_player_action == "feuille" and last_computer_action == "ciseaux":
        print("Votre pauvre feuille se fait découper par les ciseaux ennemis !")
        round_loss()
    elif last_player_action == "ciseaux" and last_computer_action == "pierre":
        print("La pierre ennemie éclate vos ciseaux !")
        round_loss()
    elif last_player_action == "ciseaux" and last_computer_action == "feuille":
        print("Vos ciseaux tranchants découpent la feuille en 1000 morceaux !")
        round_won()


def check_score():
    global player_score, computer_score, win, loose
    print(f"Score : {player_score} pour vous, {computer_score} pour l'ennemi !!\n")

    if player_score >= 3:
        win = True
    elif computer_score >= 3:
        loose = True

    return


while not win and not loose:

    player_choice()

    while not check_player_action():
        player_choice()

    last_computer_action = computer_choice()
    check_round_winner()
    check_score()

if win:
    print("Vous avez gagné :D !!!\n\n")
elif loose:
    print("Vous avez perdu :o !!!\n\n")

print("Appuyez sur une entrée pour quitter...")
input()