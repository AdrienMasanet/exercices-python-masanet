import os

directory_name = "test"
file_name = "results.txt"
max_number = 500


def create_folder():
    try:
        print(f"Création du dossier {directory_name}")
        os.mkdir(directory_name)
        print("Dossier créé avec succès")
    except FileExistsError:
        print("Erreur : le dossier existe déjà")
    except:
        print("Erreur lors de la création du dossier")


def create_file():
    try:
        print("Création du fichier et écriture...")
        with open(directory_name + "\\" + file_name, "w") as file:
            for i in range(max_number):
                if i % 2 != 0:
                    file.write(str(i) + "\n")
        print(f"Fichier {file_name} créé avec succès")
    except:
        print(f"Erreur lors de la création du fichier {file_name}")


def read_file():
    try:
        print(f"Lecture du fichier {file_name}...")
        with open(directory_name + "\\" + file_name, "r") as file:
            for line in file:
                print(line)
        print(f"Fin de la lecture du fichier {file_name}")
    except:
        print(f"Erreur lors de la lecture du fichier {file_name}")


if os.path.isdir(directory_name):
    print(f"Dossier \"{directory_name}\" trouvé")
    create_file()
    read_file()
else:
    print(f"Dossier \"{directory_name}\" introuvable")
    create_folder()
    create_file()

input()