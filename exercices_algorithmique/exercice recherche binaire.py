import random

# Passer à True pour pouvoir voir les étapes de l'algorithme dans le terminal
debug = True
# Création, remplissage et tri de la liste
list_size = 500
list = []

if debug:
    print("Remplissage de la liste en cours...")

for i in range(list_size):
    if debug:
      print(f"{i}/{list_size}", end='\r')
    list.append(i+50)

if debug:
    print("Liste remplie !\n")

list.sort()


def binary_search(sublist, target, found_number=None):

    if debug:
        print("\nAppuyez sur entrée pour commencer prochaine itération...")
        input()

    found_number = sublist[len(sublist)//2]

    if debug:
        print(f"Nombre visé: {target} Nombre trouvé : {found_number}\n")
        print(f"Sous-liste : {sublist}\n\n")

    if found_number > target:
        return binary_search(sublist[0:len(sublist)//2], target, found_number)
    elif found_number < target:
        return binary_search(sublist[len(sublist)//2:len(sublist)], target, found_number)
    elif found_number == target:
        return print(f"{found_number} trouvé dans la liste d'origine ici : {sublist} !")


if debug:
    print(list)

binary_search(list, random.randint(min(list), max(list)))
input()
