import random

grid = {
    "A1": " ",
    "B1": " ",
    "C1": " ",
    "A2": " ",
    "B2": " ",
    "C2": " ",
    "A3": " ",
    "B3": " ",
    "C3": " ",
}


def draw_grid():
    print("  ", "1", " ", "2", " ", "3")
    print("")
    print("A ", grid["A1"], "|", grid["A2"], "|", grid["A3"])
    print("  ", "――――――――")
    print("B ", grid["B1"], "|", grid["B2"], "|", grid["B3"])
    print("  ", "――――――――")
    print("C ", grid["C1"], "|", grid["C2"], "|", grid["C3"])
    print("")


def check_grid(symbol):
    print(list(grid.values())[::4]==symbol)
    #if grid[::4]==symbol:
    #    return True
    #else:
    #    return


def main():

    player1_name = "Joueur 1"
    player1_score = 0
    player2_name = "Joueur 2"
    player2_score = 0
    turn_player = random.randint(0, 1)
    turn_number = 0
    win = False

    while not win:

        turn_number += 1
        print(f"Tour n° {turn_number} !")

        if turn_player == 0:
            print(f"Au tour de {player1_name} !!")
        elif turn_player == 1:
            print(f"Au tour de {player2_name} !!")

        draw_grid()

        cell_choice = input("Écrivez le nom de la case à cocher : ")

        # Tant que le nom de la cellule est incorrect on redemande au joueur de l'écrire
        while not cell_choice in grid:
            cell_choice = input("Erreur dans le nom de la case à cocher ! Veuillez réessayer : ")

        if grid[cell_choice] == " ":
          if turn_player == 0:
              grid[cell_choice] = "O"
          elif turn_player == 1:
              grid[cell_choice] = "X"

        draw_grid()

        if check_grid("O"):
            winner_name = player1_name
            win = True
        elif check_grid("X"):
            winner_name = player2_name
            win = True

        # On change de joueur pour le prochain tour
        if turn_player == 0:
            turn_player = 1
        else:
            turn_player = 0

    print(f"{winner_name} a gagné !!!!!")
    print("Fin de la partie !")


main()
