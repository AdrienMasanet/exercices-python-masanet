class Vehicle:
    _name = ""
    _speed = 0
    _fuel = 0

    def __init__(self, name, speed, fuel):
        self._name = name
        self._speed = speed
        self._fuel = fuel

    def move(self, speed):
        print(f"Moving {self._name} at {self._speed} mph")


class Grounded():
    pass


class Flying():
    _max_altitude = 0
    _current_altitude = 0


class Floating():
    _carry_weight = 0


# Véhicules terrestres
class Train(Vehicle, Grounded):
    pass


class Car(Vehicle, Grounded):
    pass


# Véhicules aériens
class Plane(Vehicle, Flying):
    pass


class Helicopter(Vehicle, Flying):
    pass


class Rocket(Vehicle, Flying):
    pass


# Véhicules marins
class Trawler(Vehicle, Floating):
    pass


class Cargo(Vehicle, Floating):
    pass
