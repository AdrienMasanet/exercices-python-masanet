import requests
from requests.exceptions import RequestException
from requests.models import HTTPError


posts_url = "https://jsonplaceholder.typicode.com/posts"

# Get tous les posts
get_posts_request = None
try:
    get_posts_request = requests.get(posts_url)
    get_posts_request.raise_for_status()

    print(get_posts_request.json())
except HTTPError as err:
    print("Erreur HTTP :", err)
except RequestException as err:
    print("Erreur de la requête :", err)


# Post un post
post_posts_request = None
new_post = {
    "title": "foo",
    "body": "bar",
    "userId": 1
}

try:
    post_posts_request = requests.post(posts_url, params=new_post, headers={"Content-type": "application/json; charset=UTF-8"})
    post_posts_request.raise_for_status()
    print(post_posts_request.json())
except HTTPError as err:
    print("Erreur HTTP :", err)
except RequestException as err:
    print("Erreur de la requête :", err)


# Post un ou des fichiers
post_files_request = None
files_url = "https://httpbin.org/post"

files = {
    'file_1': open('fake_bin.zzz', 'rb'),
    'file_2': open('le nez.txt', 'rb')
}

try:
    post_files_request = requests.post(files_url, files=files)
    post_files_request.raise_for_status()
    print(post_files_request.json())
except HTTPError as err:
    print("Erreur HTTP :", err)
except RequestException as err:
    print("Erreur de la requête :", err)
