from flask import Flask

app = Flask(__name__)


@app.route("/")
def hello_world():
    return {
      "users": [
        {
          "id": 5,
          "name": "abdelaziz",
          "métier": "jedi"
        },        
        {
          "id": 2,
          "name": "bernard",
          "métier": "franc-maçon"
        },        
        {
          "id": 9,
          "name": "simon",
          "métier": "jongleur"
        }
      ]
    }
