import psycopg2

try:
    db_connection = psycopg2.connect(
        dbname="database",
        user="root",
        password="root"
    )
    print("Connection à la base de données effectuée avec succès")

    try:
        cursor = db_connection.cursor()
        cursor.execute("""
        CREATE TABLE users(
        id SERIAL,
        name VARCHAR(100),
        surname VARCHAR(100),
        email VARCHAR(255),
        birth_date DATE
        )""")
        db_connection.commit()
        print("Table users créée avec succès")
    except Exception as error:
        print("Erreur lors de la création de la table users", error)
        db_connection.close()

except Exception as error:
    print("Erreur lors de la connection à la base de données :", error)
