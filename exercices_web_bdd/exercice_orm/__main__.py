from sqlalchemy import create_engine, Column, Integer, String, exc
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String)
    surname = Column(String)
    email = Column(String)

    def __init__(self, name, surname, email):
        self.name = name
        self.surname = surname
        self.email = email

    def print_name(self):
        print(f"{self.name}, {self.surname}, {self.email}")


try:
    print("Connection à la bdd")
    engine = create_engine("postgresql://root:root@127.0.0.1:5432/database")
    engine.connect()
    Base.metadata.create_all(engine)
    print("Bdd connectée avec succès")
    Session = sessionmaker(engine)
    session = Session()
    print("Création du nouvel utilisateur")
    user_1 = User("Adrien", "Masanet", "adrien.masanet@hotmail.fr")
    print("Nouvel utilisateur créé avec succès")
    print("Enregistrement du nouvel utilisateur en bdd")
    session.add(user_1)
    session.commit()
    print("Nouvel utilisateur enregistré dans la bdd avec succès")
except Exception as error:
    print("Erreur :", error)
