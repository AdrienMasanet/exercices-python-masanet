# Importe la CLASSE user dans le FICHIER Models/User.py
from Models.User import User
from datetime import datetime

def create_users(database):
  for i in range(20):
    new_user = User(name='guest', surname='guest', email='guest@example.com', birth_date=datetime.now())
    database.session.add(new_user)
    database.session.commit()

  return "<p>ok !!</p>"