from api import database, marshmallow
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema


class User(database.Model):
    __tablename__ = "users"

    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    name = database.Column(database.String)
    surname = database.Column(database.String)
    email = database.Column(database.String)
    birth_date = database.Column(database.Date)

    def __init__(self, name, surname, email, birth_date):
        self.name = name
        self.surname = surname
        self.email = email
        self.birth_date = birth_date

    def print_infos(self):
        print(f"{self.name}, {self.surname}, {self.email}, {self.birth_date}")

class UserSchema(marshmallow.SQLAlchemyAutoSchema):
  class Meta:
    model = User