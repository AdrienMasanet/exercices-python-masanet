import os
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

api = Flask(__name__)
api.config["SQLALCHEMY_DATABASE_URI"] = f"postgresql://{os.environ['DB_USER']}:{os.environ['DB_PASSWORD']}@postgres:{os.environ['DB_PORT']}/{os.environ['DB_NAME']}"

database = SQLAlchemy(api)
marshmallow = Marshmallow(api)

# Import des modèles
from Models import User
from Models.User import User, UserSchema

# Import des fixtures après l'importation des modèles
from fixtures import create_users

# Créé et commit les tables dans la base de données
database.create_all()


@api.route("/")
def index():
    return "<p>Accueil de l'API !</p>"


@api.route("/user/<int:id>", methods=["GET"])
def get_user(id):
    user = User.query.filter_by(id=id).first()
    user_schema = UserSchema()
    return user_schema.dump(user)


@api.route("/users", methods=["GET"])
def get_all_users():
    users = User.query.all()
    user_schema = UserSchema(many=True)
    return jsonify(user_schema.dump(users))


@api.route("/create-fixture-users")
def create_fixture_users():
    create_users(database)
    return "<p>Utilisateurs créés ;) !</p>"
