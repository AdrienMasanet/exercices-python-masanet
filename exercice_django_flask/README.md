# DOCKER :

Les fichiers "dockerfile" (sans extension) sont utilisés pour créer une image.

docker compose build pour build un dossier existant, aussi utlisé pour rebuild après modification du docker-compose.yaml.

docker compose pull pour télécharger ou re-télécharger les images définies qui sont téléchargées sur le HUB Docker.

docker compose-up pour télécharger/build pour la première fois puis lancer les conteneurs listés en tant que services dans le docker-compose.yaml

! ATTENTION ! ne pas oublier que docker tourne sur le localhost, les services sont donc accédés depuis localhost:port




# PARAMETRES DU DOCKER-COMPOSE.yml :

"volumes" permet de dire au container de partager un ou des dossiers avec l'ordinateur, très important pour la persistance des données ou les changements en temps réel car lorsque le conteneur s'arrête les données sont perdues

"restart" permet de gérer le comportement du service lors de l'arrêt naturel ou par bug du service, on peut définir un nombre de redémarrages ou un paramètre "always"

"depends_on" permet de spécifier un ou des services qui douvent démarrer avant celui-ci, attention le fait qu'un autre service démarre avant ne veut pas forcément dire qu'il est prêt à être consommé lorsque le service qui en dépend se lance

"env_file" permet de spécifier le chemin d'accès du fichier contenant les variables d'environnement du service. Il est possible pour plusieurs services d'utiliser le même fichier .env ou bien que chaque service ait sont fichier .env séparé

"networks" permet de définir une groupe pour lier d'une façon précise les différents services. On peut voir tout en bas du cocker-compose.yaml qu'on définit le "driver" en "bridge" qui simule une connexion internet entre les deux services afin qu'ils puissent communiquer. 

"ports: XYZA:XYZA" permet de portforwarder le port du localhost sur le port du service afin que les deux ports représentent la même chose pour les deux