from django.shortcuts import render
from .models import Article
import requests
from requests.exceptions import RequestException
from requests.models import HTTPError


def index(request):
    return render(request, "front/index.html", {
        "page_title": "Accueil"
    })

def articles_list(request):

    articles = Article.objects.all()

    return render(request, "front/articles_list.html", {
        "page_title": "Articles",
        "articles": articles
    })

def article_detail(request, id):

    try:
      article = Article.objects.get(id=id)
    except:
      article = False

    return render(request, "front/article_detail.html", {
        "page_title": "Lecture d'un article",
        "article": article
    })
    
def users_list(request):

    users = False

    # TODO : GET USERS FROM API AND PUT THE RESUT IN "users" VARIABLE
    try:
      get_posts_request = requests.get("http://localhost:5000/users")
      get_posts_request.raise_for_status()

      users=get_posts_request.json()
    except HTTPError as err:
      print("Erreur HTTP :", err)
    except RequestException as err:
      print(get_posts_request.json())
    except HTTPError as err:
      print("Erreur HTTP :", err)
    except RequestException as err:
      print("Erreur de la requête :", err)

    return render(request, "front/users_list.html", {
        "page_title": "Liste des utilisateurs enregistrés sur l'API",
        "users": users
    })