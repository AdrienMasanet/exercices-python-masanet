from django.db import models

class User(models.Model):
  name = models.CharField(max_length=50)
  surname = models.CharField(max_length=50)
  email = models.CharField(max_length=100)
  birth_date = models.DateField()

class Article(models.Model):
  author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
  title = models.CharField(max_length=100)
  content = models.TextField()
  publication_date = models.DateField()

class Comment(models.Model):
  article = models.ForeignKey(Article, null=True, on_delete=models.SET_NULL)
  author = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)
  content = models.TextField()
  publication_date = models.DateField()